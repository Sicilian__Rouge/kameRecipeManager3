package kame.kameRecipeManager.listener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.utils.stand.StandUtils;

public class CraftListener implements Listener {

	private static final ItemStack DEFAULT_ITEM;
	private static HashMap<UUID, ArmorStand> stands = new HashMap<>();


	static {
		ItemStack item = new ItemStack(Material.COOKED_CHICKEN);
		ItemMeta im = item.getItemMeta();
		im.setDisplayName("\1");
		im.setLore(Arrays.asList("@craft", "@inhand"));
		item.setItemMeta(im);
		DEFAULT_ITEM = item;
	}



	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryInteract(PlayerInteractEvent event) {
		switch(event.getAction()) {
		case RIGHT_CLICK_BLOCK:
			if(onBlockClick(event))return;
		case RIGHT_CLICK_AIR:
			if(onAirClick(event))return;
		default:
			break;
		}
	}

	private boolean onAirClick(PlayerInteractEvent event) {
		ItemStack item = event.getItem().clone();
		if(item == null || !item.getItemMeta().hasLore())return false;
		String lore = item.getItemMeta().getLore().toString().toLowerCase();
		if(lore.contains("@craft") && lore.contains("@inhand")) {
			Player player = event.getPlayer();
			if(!item.getItemMeta().hasDisplayName())item = DEFAULT_ITEM;
			ArmorStand entity = StandUtils.createStand(player.getLocation(), item);
			entity.setLeggings(DEFAULT_ITEM);
			stands.put(player.getUniqueId(), entity);
			Main.cast(Main.COLOR + "d[鯖たん] §e手持ち作業台にゃん♪(ฅ•ω•ฅ)");
			event.setCancelled(true);
			return true;
		}
		return false;
	}

	private boolean onBlockClick(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();
		ArmorStand entity = StandUtils.getCraftStand(block);
		if(event.isBlockInHand() && player.isSneaking()) {
			return false;
		}

		if(entity == null)return false;
		switch(block.getType()) {
		case FURNACE:
		case BURNING_FURNACE:
			stands.put(player.getUniqueId(), entity);
		case BREWING_STAND:
			//onBrew(entity, player);
			event.setCancelled(true);
			return true;
		case ANVIL:
			stands.put(player.getUniqueId(), entity);
			return true;
		case WORKBENCH:
			stands.put(player.getUniqueId(), entity);
			return true;
		default:
			player.openWorkbench(block.getLocation(), true);
			stands.put(player.getUniqueId(), entity);
			event.setCancelled(true);
			return true;
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void openInventory(InventoryOpenEvent event) {

	}

	@EventHandler
	private void closeInventory(InventoryCloseEvent event) {
		if(stands.containsKey(event.getPlayer().getUniqueId())) {
			Main.cast(Main.COLOR + "d[鯖たん] インベントリ閉じたにゃん！(ฅ•ω•ฅ)");
			ArmorStand stand = stands.remove(event.getPlayer().getUniqueId());
			if(stand.getLocation().getBlock().getType() == Material.AIR ||
					stand.getLeggings().equals(DEFAULT_ITEM))stand.remove();
		}
	}



}
