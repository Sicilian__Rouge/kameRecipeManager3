package kame.kameRecipeManager.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.utils.stand.StandUtils;

public class BlockListener implements Listener {
	
	private class Drops {
		private final ArmorStand stand;
		private Entity sand;
		private Drops(ArmorStand stand, Entity entity) {
			this.stand = stand;
			this.sand = entity;
		}
		
		private boolean prepareDrop() {
			if(sand.isDead()) {
				if(stand.getWorld().getGameRuleValue("doTileDrops").equals("true"))
					sand.getWorld().dropItem(sand.getLocation(), stand.getChestplate());
				stand.getWorld().playSound(sand.getLocation(), Sound.ENTITY_ARMORSTAND_FALL, 1, 1);
				stand.remove();
				return true;
			}
			return false;
		}
		public String toString() {
			return this.sand + " " + this.stand;
			
		}
	}
	private static ArrayList<Drops> drops = new ArrayList<>();
	private static BukkitRunnable run = new BukkitRunnable() {
		public void run() {
			drops.removeIf(x -> x.prepareDrop());
		}
	};
	static {
		run.runTaskTimer(Main.getInstance(), 0, 1);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	private void onBlockPlace(BlockPlaceEvent event) {
		switch(event.getBlockReplacedState().getType()) {
		case AIR:
		case WATER:
		case LAVA:
		case STATIONARY_WATER:
		case STATIONARY_LAVA:
		case DIRT:
		case GRASS:
		case DOUBLE_PLANT:
		case LONG_GRASS:
			if(event.isCancelled())return;
			break;
		default:
			return;
		}
		ItemStack item = event.getItemInHand().clone();
		ItemStack check = new ItemStack(item.getType(), item.getAmount(), item.getDurability());
		if(item.equals(check))return;
		item.setAmount(1);
		Location loc = event.getBlock().getLocation().add(0.5, Main.getOffsetY(), 0.5);
		ArmorStand stand = StandUtils.createStand(loc, item);
		ItemMeta meta = item.getItemMeta();
		if(meta.hasLore() && meta.hasDisplayName()) {
			String lore = meta.getLore().toString();
			if(	lore.contains("@craft")  || 
				lore.contains("@furnace")|| 
				lore.contains("@fusion") || 
				lore.contains("@smash")) {
				StandUtils.setCraftStand(stand, item, lore.contains("@hide"));
				loc.getWorld().playSound(loc, Sound.ENTITY_ITEMFRAME_PLACE, 1, 1);
			}
		}
		new BukkitRunnable() {
			public void run() {
				stand.teleport(loc);
			}
		}.runTaskLater(Main.getInstance(), 2);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void onBlockChange(EntityChangeBlockEvent event){
		Entity entity = event.getEntity();
		if(entity instanceof FallingBlock) {
			if(!entity.isOnGround()) {
				ArmorStand stand = StandUtils.getKameStand(event.getBlock());
				if(stand == null)return;
				((FallingBlock) entity).setDropItem(false);
				entity.setPassenger(stand);
				drops.add(new Drops(stand, entity));
			}else {
				drops.stream().filter(x -> x.sand.equals(entity)).forEach(x ->  {
					x.stand.teleport(event.getBlock().getLocation().add(0.5, Main.getOffsetY(), 0.5));
					x.stand.getWorld().playSound(x.stand.getLocation(), Sound.ENTITY_ARMORSTAND_HIT, 1, 1);
				});
				drops.removeIf(x -> x.sand.equals(entity));
			}
		}else {
			if(StandUtils.getKameStand(event.getBlock()) == null)return;
			event.setCancelled(true);
			event.getBlock().getState().update();
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	private void onBlockPiston(BlockPistonExtendEvent event) {
		onPistonMove(event, event.getBlocks());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onBlockPiston(BlockPistonRetractEvent event) {
		onPistonMove(event, event.getBlocks());
	}

	private void onPistonMove(BlockPistonEvent event, List<Block> blocks) {
		if (event.isCancelled())return;
		HashMap<ArmorStand, Location> list = new HashMap<>();
		BlockFace b = event.getDirection();
		for(Block block : blocks) {
			ArmorStand stand = StandUtils.getKameStand(block);
			if(stand == null)continue;
			switch(block.getPistonMoveReaction()) {
			case BREAK:
				blockremove(block, stand);
				break;
			case MOVE:
				list.put(stand, stand.getLocation().add(b.getModX(), b.getModY(), b.getModZ()));
				if(StandUtils.isCraftStand(stand))block.getWorld().playSound(stand.getLocation(), Sound.ENTITY_ITEMFRAME_ROTATE_ITEM, 1, 1);
				break;
			case BLOCK:
			default:
				break;
			}
		}
		new BukkitRunnable() {
			public void run() {
				list.entrySet().forEach(entry -> entry.getKey().teleport(entry.getValue()));
			}
		}.runTaskLater(Main.getInstance(), 2);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void onBlockBreak(EntityExplodeEvent event) {
		if(event.isCancelled())return;
		for(Block block : event.blockList()) {
			ArmorStand stand = StandUtils.getKameStand(block);
			if (stand != null)blockremove(block, stand);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.MONITOR)
	private void onBlockBreak(BlockBreakEvent event) {
		if(event.isCancelled())return;
		Block block = event.getBlock();
		ArmorStand entity = StandUtils.getKameStand(block);
		if(entity == null)return;
		Location loc = block.getLocation();
		Player player = event.getPlayer();
		if(!player.getGameMode().equals(GameMode.CREATIVE) && block.getDrops(player.getInventory().getItem(player.getInventory().getHeldItemSlot())).size() > 0) {
			loc.getWorld().dropItemNaturally(loc.add(0.5, 0, 0.5), entity.getChestplate());
		}
		if(StandUtils.isCraftStand(entity))block.getWorld().playSound(block.getLocation().add(0.5,0.5,0.5), Sound.ENTITY_ITEMFRAME_BREAK, 1, 1);
		loc.getWorld().getPlayers().stream().filter(p -> p.getLocation().distance(loc) < 16 && p != player).forEach(p -> 
			p.spigot().playEffect(loc, Effect.STEP_SOUND, block.getType().ordinal(), block.getData(), 0, 0, 0, 0, 1, 16));
		block.setType(Material.AIR);
		entity.remove();
	}

	private void blockremove(Block block, ArmorStand stand) {
		if (block.getWorld().getGameRuleValue("doTileDrops").equals("true") && block.getDrops().size() > 0) {
			block.getWorld().dropItemNaturally(block.getLocation().add(0.5, 0, 0.5), stand.getChestplate());
		}
		if(StandUtils.isCraftStand(stand))block.getWorld().playSound(block.getLocation().add(0.5,0.5,0.5), Sound.ENTITY_ITEMFRAME_BREAK, 1, 1);
		block.setType(Material.AIR);
		stand.remove();
	}

}
