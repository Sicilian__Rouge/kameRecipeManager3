package kame.kameRecipeManager.reader;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.jar.JarFile;

import org.bukkit.Bukkit;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import com.google.common.collect.Lists;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.KFurnaceRecipe;
import kame.kameRecipeManager.recipe.KShapedRecipe;
import kame.kameRecipeManager.recipe.KShapelessRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;


public class FileReader {
	private static final File DIR = Main.getInstance().getDataFolder();

	private class Filter implements FileFilter {
		public boolean accept(File path) {
			return path.getName().endsWith(".txt");
		}
	}

	private FileReader() {
		long time = System.currentTimeMillis();
		List<Recipe> recipes = Lists.newArrayList(Bukkit.recipeIterator());
		KameRecipes.clear();
		System.err.println("<kameRecipe> =======kameRecipeManager========");
		System.err.println("<kameRecipe> Thank you for using this plugin");
		System.err.println("<kameRecipe> this plugin powerd by kameme");
		System.err.println("<kameRecipe> ================================");
		try {
			loadDirectory();
		} catch (IOException e) {
			e.printStackTrace();
			Bukkit.getLogger().severe("<kameRecipeManager> ====================ERROR======================");
			Bukkit.getLogger().severe("<kameRecipeManager> 何らかの原因でファイルの読み込みに失敗しました!");
			Bukkit.getLogger().severe("<kameRecipeManager> " + e.getMessage());
			Bukkit.getLogger().severe("<kameRecipeManager> ====================ERROR======================");
		}
		loadDefault(recipes);
		System.err.println("<kameRecipe> load recipe " + (float) (System.currentTimeMillis() - time) / 1000.0F + "[s]");
	}
	
	public static void readRecipes() {
		new FileReader();
	}

	private void defaultCopy() throws UnsupportedEncodingException, FileNotFoundException, IOException {
		String path[] = getClass().getResource("sample.txt").getPath().split("!");
		JarFile f = new JarFile(new File(path[0].substring(6)));
		InputStreamReader reader = new InputStreamReader(f.getInputStream(f.getEntry(path[1].substring(1))), "UTF-8");
		try(OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(DIR, "/recipes/sample.txt")), "UTF-8")) {
			while(reader.ready())writer.write(reader.read());
		}
		f.close();
	}

	private void loadDirectory() throws IOException {
		File files = new File(DIR, "/recipes");
		if(!files.exists())files.mkdirs();
		File filter[] = files.listFiles(new Filter());
		if(filter.length == 0) {
			defaultCopy();
			System.err.println("<kameRecipeManager> clone sample recipe!");
			filter = files.listFiles(new Filter());
		}
		StringBuilder builder = new StringBuilder("[kameRecipeManager] detected " + filter.length + " files {");
		for(File f : filter)builder.append(" ").append(f.getName());
		System.out.println(builder.append(" }").toString());
		FileParser parse = new FileParser();
		for(File file : filter) {
			parse.parseFile(file);
		}
		parse.finalyze();
	}
	
	public void loadDefault(List<Recipe> recipes) {
		for(Recipe r : recipes) {
			if((r instanceof ShapedRecipe)) {
				if(((ShapedRecipe) r).getIngredientMap().size() == 1) {
					ShapelessRecipe re = new ShapelessRecipe(r.getResult());
					re.addIngredient(((ShapedRecipe) r).getIngredientMap().values().iterator().next().getData());
				KameRecipes.addRecipe("COMBINE:クラフト", new KShapelessRecipe(re, re.getIngredientList(), new ArrayList<>(), new ArrayList<>(), new HashSet<>()));
				}else {
					ShapedRecipe re = (ShapedRecipe) r;KameRecipes.addRecipe("CRAFT:クラフト", new KShapedRecipe(re, re.getIngredientMap(), new ArrayList<>(), new ArrayList<>(), new HashSet<>()));
				}
			}else if((r instanceof ShapelessRecipe)) {
				ShapelessRecipe re = (ShapelessRecipe) r;
				KameRecipes.addRecipe("COMBINE:クラフト", new KShapelessRecipe(re, re.getIngredientList(), new ArrayList<>(), new ArrayList<>(), new HashSet<>()));
			}else if((r instanceof FurnaceRecipe)) {
				FurnaceRecipe re = (FurnaceRecipe) r;
				KameRecipes.addRecipe("SMELT:かまど", new KFurnaceRecipe(re, re.getInput(), 80, new CraftingEffect(), new CraftingSound(), new ArrayList<>(), new ArrayList<>(), new HashSet<>()));
			}
		}
	}

}
