package kame.kameRecipeManager.reader.exception;

public class IllegalItemException extends Exception {
	public enum ErrorType {
		MATERIAL,
		NUMBER,
		AUGMENTS,
		ENCHANTMENTS,
		BADNBT,
		OTHER,
		WARN
	}

	private final ErrorType type;
	private final String error;
	private final String message;

	public IllegalItemException(ErrorType type, String e, Object... message) {
		this.type = type;
		this.error = e;
		StringBuilder b = new StringBuilder();
		for(Object o : message)b.append(o);
		this.message = b.toString();
	}

	public ErrorType getType() {
		return type;
	}

	public String getError() {
		return error;
	}

	public String getMessages() {
		return message;
	}

}
