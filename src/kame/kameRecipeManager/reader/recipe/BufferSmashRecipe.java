package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KSmashRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.utils.RecipeType;

public class BufferSmashRecipe extends BufferedRecipe {

	private ArrayList<ItemStack> input = new ArrayList<>();
	private String spell;
	private CraftingEffect effect = new CraftingEffect();
	private CraftingSound sound = new CraftingSound();

	public BufferSmashRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Smash;
	}

	private boolean addSmash(ItemStack item) {
		return input.add(item);
	}

	private boolean setSpell(String spell) {
		if(this.spell == null) {
			this.spell = spell;
			return true;
		}else {
			return false;
		}
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		if(line.matches("I[1-9 ]= .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(!addSmash(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
			}
		}
		if(line.matches("=== .*")) {
			if(!super.setResult(parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "ResultItemError ", " already set items!");
		}
		if(line.matches("\\$option @.*")) {
			for(String opt : line.substring(9).split(" @"))super.addOption(opt);
		}
		if(line.matches("\\$checkmode *")) {
			for(String ckm : line.substring(11).split(" "))super.addCheckMode(CheckMode.fromName(ckm));
		}
		if(line.matches("\\$spell:*")) {
			if(!setSpell(line.substring(7)))
				throw new IllegalItemException(ErrorType.OTHER, line, "SmashSpellError ", " already set spell!");
		}
		if(line.matches("\\$effect:[0-z:_\\.]+")) {
			line = line.substring(8);
			String str[] = line.split(":", 2);
			effect = new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.matches("\\$sound:[0-z:_]+")) {
			line = line.substring(7);
			String str[] = line.split(":", 2);
			sound = new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
	}

	@Override
	public void createRecipe() {
		KameRecipes.addRecipe("SMASH:".concat(recipename), new KSmashRecipe(null, input, spell, effect, sound, options, products, checks));
	}

	@Override
	public boolean isReady() {
		return type == RecipeType.Smash && spell != null && input.size() > 0 && result != null;
	}
}
