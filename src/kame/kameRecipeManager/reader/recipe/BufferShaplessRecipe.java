package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KShapelessRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.utils.RecipeType;

public class BufferShaplessRecipe extends BufferedRecipe {

	private List<ItemStack> items = new ArrayList<ItemStack>();

	public BufferShaplessRecipe(String recipename) {
		super(recipename);
		super.type = RecipeType.Shapeless;
	}

	private boolean addShapeless(ItemStack item) {
		return items.size() < 10 ? items.add(item) : false;
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		if(line.matches("I[1-9 ]= .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(!addShapeless(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
			}
		}
		if(line.matches("=== .*")) {
			if(!super.setResult(parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "ResultItemError ", " already set items!");
		}
		if(line.matches("\\$option @.*")) {
			for(String opt : line.substring(9).split(" @"))super.addOption(opt);
		}
		if(line.matches("\\$checkmode *")) {
			for(String ckm : line.substring(11).split(" "))super.addCheckMode(CheckMode.fromName(ckm));
		}
	}

	@Override
	public void createRecipe() {
		ShapelessRecipe recipe = new ShapelessRecipe(result);
		for(ItemStack item : items)recipe.addIngredient(item.getData());
		KameRecipes.addRecipe("COMBINE:".concat(recipename), new KShapelessRecipe(recipe, items, options, products, checks));
	}

	@Override
	public boolean isReady() {
		return type == RecipeType.Shapeless && items.size() > 0 && result != null;
	}
}
