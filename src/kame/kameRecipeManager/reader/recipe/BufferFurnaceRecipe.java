package kame.kameRecipeManager.reader.recipe;

import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.reader.utils.Utils;
import kame.kameRecipeManager.recipe.KFurnaceRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.utils.RecipeType;

public class BufferFurnaceRecipe extends BufferedRecipe {

	private ItemStack input;
	private int time = -80;
	private float exp;
	private CraftingEffect effect = new CraftingEffect();
	private CraftingSound sound = new CraftingSound();

	public BufferFurnaceRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Furnace;
	}

	private boolean setInput(ItemStack item) {
		return this.input == null ? (input = item) != null : false;
	}

	private boolean setTimes(int time) throws IllegalItemException {
		if(time <= 0)throw new IllegalItemException(ErrorType.OTHER, "", "FurnaceTimeError ", time , " is smaller! the number bigger than 0!");
		return this.time == -80 ? (this.time = time) != 0 : false;
	}

	private boolean setExp(float exp) throws IllegalItemException {
		if(exp <= 0)throw new IllegalItemException(ErrorType.OTHER, "", "FurnaceExpError ", exp , " is smaller! the number bigger than 0!");
		return this.exp == 0 ? (this.exp = exp) != 0 : false;
	}


	@Override
	public void parseLine(String line) throws IllegalItemException {
		if(line.matches("I = .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			if(!setInput(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
		}
		if(line.matches("=== .*")) {
			if(!super.setResult(parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "ResultItemError ", " already set items!");
		}
		if(line.matches("\\$option @.*")) {
			for(String opt : line.substring(9).split(" @"))super.addOption(opt);
		}
		if(line.matches("\\$checkmode *")) {
			for(String ckm : line.substring(11).split(" "))super.addCheckMode(CheckMode.fromName(ckm));
		}
		if(line.matches("\\$times:[0-9]+")) {
			if(!setTimes(Utils.toInt(line.substring(7), 100)))
				throw new IllegalItemException(ErrorType.OTHER, line, "FurnaceTimelError ", " already set times!");
		}
		if(line.matches("\\$exp:[0-9]+(.[0-9]+)?")) {
			if(!setExp(Float.parseFloat(line.substring(5))))
				throw new IllegalItemException(ErrorType.OTHER, line, "FurnaceExpError ", " already set times!");
		}
		if(line.matches("\\$effect:[0-z:_\\.]+")) {
			line = line.substring(8);
			String str[] = line.split(":", 2);
			effect = new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.matches("\\$sound:[0-z:_]+")) {
			line = line.substring(7);
			String str[] = line.split(":", 2);
			sound = new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
	}

	@Override
	public void createRecipe() {
		FurnaceRecipe recipe = new FurnaceRecipe(result, input.getData(), exp);
		KameRecipes.addRecipe("SMELT:".concat(recipename), new KFurnaceRecipe(recipe, input, time == -80 ? 80 : time, effect, sound, options, products, checks));
	}

	@Override
	public boolean isReady() {
		return type == RecipeType.Furnace && input != null && result != null;
	}

}
