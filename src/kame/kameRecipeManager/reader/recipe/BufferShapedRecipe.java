package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KShapedRecipe;
import kame.kameRecipeManager.recipe.KShapelessRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.utils.RecipeType;

public class BufferShapedRecipe extends BufferedRecipe{

	private ArrayList<String> shapes = new ArrayList<String>();
	private HashMap<Integer, ItemStack> rawmap = new HashMap<>();
	private HashMap<Character, ItemStack> map = new HashMap<>();
	private int sizeX = 0;
	private int sizeY = 0;

	public BufferShapedRecipe(String recipename) {
		super(recipename);
		super.type = RecipeType.Shaped;
	}

	private boolean addShape(String shape) {
		if(shape.length() > 3)return false;
		sizeX = Math.max(sizeX, shape.length());
		if(sizeY < 3)sizeY++;
		return shapes.size() < 3 ? shapes.add(shape) : false;
	}

	private boolean setItem(int key, ItemStack item) {
		return rawmap.containsKey(key) ? false : rawmap.put(key, item) == null;
	}

	private int[] parseRecipe() {
		int[] k = new int[sizeX * sizeY];
		for(int i = 0; i < sizeY; i++) {
			char c[] = shapes.get(i).toCharArray();
			for(int j = 0; j < sizeX; j++) {
				k[i*sizeX + j] = j < c.length ? c[j]-'0' : 0;
			}
		}
		return k;
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		if(line.matches("[0-9]( *\\+ *[0-9])?( *\\+ *[0-9])? *")) {
			if(!this.addShape(line.replaceAll("[^0-9]", "")))
				throw new IllegalItemException(ErrorType.OTHER, line, "ShapeError ", " too many shapes! ");
		}
		if(line.matches("[1-9] = .*")) {
			if(!this.setItem(line.charAt(0)-48, parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError [" + line.charAt(0) + "] already set items! ");
		}
		if(line.matches("=== .*")) {
			if(!super.setResult(parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "ResultItemError ", " already set items!");
		}
		if(line.matches("\\$option @.*")) {
			for(String opt : line.substring(9).split(" @"))super.addOption(opt);
		}
		if(line.matches("\\$checkmode *")) {
			for(String ckm : line.substring(11).split(" "))super.addCheckMode(CheckMode.fromName(ckm));
		}
	}

	@Override
	public void createRecipe() {
		if(rawmap.size() == 1) {
			ShapelessRecipe recipe = new ShapelessRecipe(result);
			List<ItemStack> items = new ArrayList<>(rawmap.values());
			for(ItemStack item : items)recipe.addIngredient(item.getData());
			KameRecipes.addRecipe("CRAFT:".concat(recipename), new KShapelessRecipe(recipe, items, options, products, checks));
		}else {ShapedRecipe recipe = new ShapedRecipe(result);
		recipe.shape(new String[] { "abc", "def", "ghi" });
		int[] cara = parseRecipe();
		StringBuilder builder = new StringBuilder();
		for(char c = 'a'; c < 'a'+ sizeX * sizeY ; c++) {
			builder.append(c);
			ItemStack mapitem = rawmap.get(cara);
			if(mapitem != null) {
				map.put(c, mapitem);
				recipe.setIngredient(c, mapitem.getData());
			}
			if(c % sizeX == 0)builder.append(' ');
		}
		recipe.shape(builder.toString().split(" "));
		KameRecipes.addRecipe("CRAFT:".concat(recipename), new KShapedRecipe(recipe, map, options, products, checks));
		}

	}

	public boolean isReady() {
		return type == RecipeType.Shaped && shapes.size() > 0 && rawmap.size() > 0 && result != null;
	}
}
