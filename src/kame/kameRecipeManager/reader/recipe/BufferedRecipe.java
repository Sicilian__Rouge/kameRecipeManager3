package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.reader.utils.Utils;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.ProductRecipe;
import kame.kameRecipeManager.recipe.utils.RecipeType;

public abstract class BufferedRecipe {

	protected final String recipename;
	protected ItemStack result;
	protected ArrayList<String> options = new ArrayList<String>();
	protected ArrayList<ProductRecipe> products = new ArrayList<>();
	protected Set<CheckMode> checks = new HashSet<>();
	protected RecipeType type = RecipeType.Droped;

	protected BufferedRecipe(String recipename) {
		this.recipename = recipename;
	}

	public String getName() {
		return recipename;
	}

	public void drop() {
		type = RecipeType.Droped;
	}

	public boolean isDrop() {
		return type == RecipeType.Droped;
	}

	public RecipeType getType() {
		return type;
	}

	public boolean setResult(ItemStack item) {
		if(result == null) {
			result = item;
			return true;
		}else {
			return false;
		}
	}

	public void addOption(String option) {
		options.add(option);
	}

	public void addProduct(ProductRecipe recipe) {
		products.add(recipe);
	}

	public void addCheckMode(CheckMode check) {
		if(check != null)checks.add(check);
	}

	@SuppressWarnings("deprecation")
	protected ItemStack parseItemStack(String line) throws IllegalItemException {
		String[] metas = line.split(" @");
		Material material = Utils.parseMaterial(metas[0]);
		if(material == null)throw new IllegalItemException(ErrorType.MATERIAL, metas[0], "is inknown Material");
		String[] item = metas[0].split(":");
		short dat = item.length > 1 ? (short) Utils.toInt(line, -1) : -1;
		int amount = item.length > 2 ? amount = Utils.toInt(line, 1) : 1;
		ItemStack result = new ItemStack(material, amount);
		if(dat == -1)result.setDurability((short) 32767);
		else result.setDurability(dat);
		ItemMeta im = result.getItemMeta();
		List<String> lore = new ArrayList<String>();
		for(String meta : metas) {
			item = meta.split(":", 2);
			switch(item[0]) {
			case "name":
				im.setDisplayName(meta.substring(5));
				break;
			case "lore":
				lore.add(meta.substring(5));
				im.setLore(lore);
				break;
			case "ench":
				String[] enchs = meta.split(":");
				if(enchs.length == 3) {
					Enchantment e = enchs[1].matches("[0-9]+") ? e = Enchantment.getById(Utils.toInt(enchs[1], -1)) : Enchantment.getByName(enchs[1]);
					if(e == null)throw new IllegalItemException(ErrorType.ENCHANTMENTS, "", "", enchs[1] , " is unknown Enchant");
					im.addEnchant(e, Utils.toInt(enchs[2], -1), true);
				}else throw new IllegalItemException(ErrorType.ENCHANTMENTS, "Enchantment. too few arguments");
				break;
			case "tag":
				try {
					im = Bukkit.getUnsafe().modifyItemStack(result, meta.substring(4)).getItemMeta();
				}catch(Exception e) {
					new IllegalItemException(ErrorType.BADNBT, "NBTtag;" + meta + " is failed");
				}
				break;
			default:
			}
		}
		result.setItemMeta(im);
		return result;
	}

	public abstract void parseLine(String line) throws IllegalItemException;

	public abstract void createRecipe();

	public abstract boolean isReady();
}
