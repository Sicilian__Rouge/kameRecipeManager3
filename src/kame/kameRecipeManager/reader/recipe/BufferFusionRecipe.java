package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.reader.utils.Utils;
import kame.kameRecipeManager.recipe.KFusionRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.utils.RecipeType;

public class BufferFusionRecipe extends BufferedRecipe {

	private ArrayList<ItemStack> input = new ArrayList<>();
	private int time = -100;
	private CraftingEffect effect = new CraftingEffect();
	private CraftingSound sound = new CraftingSound();

	public BufferFusionRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Fusion;
	}

	private boolean addFusion(ItemStack item) {
		return this.input.size() < 4 ? input.add(item) : false;
	}

	private boolean setTimes(int time) throws IllegalItemException {
		if(time <= 0)throw new IllegalItemException(ErrorType.OTHER, "", "FusionTimeError ", time , " is smaller! the number bigger than 0!");
		return this.time == -100 ? (this.time = time) != 0 : false;
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		if(line.matches("I[1-3 ]= .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(!addFusion(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
			}
		}
		if(line.matches("=== .*")) {
			if(!super.setResult(parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "ResultItemError ", " already set items!");
		}
		if(line.matches("\\$option @.*")) {
			for(String opt : line.substring(9).split(" @"))super.addOption(opt);
		}
		if(line.matches("\\$checkmode *")) {
			for(String ckm : line.substring(11).split(" "))super.addCheckMode(CheckMode.fromName(ckm));
		}
		if(line.matches("\\$times:[0-9]+")) {
			if(!setTimes(Utils.toInt(line.substring(7), 100)))
				throw new IllegalItemException(ErrorType.OTHER, line, "FusionTimeError ", " already set times!");
		}
		if(line.matches("\\$effect:[0-z:_\\.]+")) {
			line = line.substring(8);
			String str[] = line.split(":", 2);
			effect = new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.matches("\\$sound:[0-z:_]+")) {
			line = line.substring(7);
			String str[] = line.split(":", 2);
			sound = new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
	}

	@Override
	public void createRecipe() {
		KameRecipes.addRecipe("FUSION:".concat(recipename), new KFusionRecipe(null, input, time == -100 ? 100 : time, effect, sound, options, products, checks));
	}

	@Override
	public boolean isReady() {
		return type == RecipeType.Fusion && input.size() > 0 && result != null;
	}

}
