package kame.kameRecipeManager.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.recipe.BufferFurnaceRecipe;
import kame.kameRecipeManager.reader.recipe.BufferFusionRecipe;
import kame.kameRecipeManager.reader.recipe.BufferShapedRecipe;
import kame.kameRecipeManager.reader.recipe.BufferShaplessRecipe;
import kame.kameRecipeManager.reader.recipe.BufferSmashRecipe;
import kame.kameRecipeManager.reader.recipe.BufferedRecipe;

public class FileParser {
	private long time = System.currentTimeMillis();
	private int LINE;
	private int READ;
	private int WARN;
	private String filename;
	private static int FAIL, FINE;
	private BufferedRecipe buffer;
	FileParser() {
		LINE = FAIL = FINE = 0;
		System.out.println("<kameRecipe> ===============================================");
	}
	public void finalyze() {
		System.out.println("<kameRecipe> load recipe " + (System.currentTimeMillis() - time) / 1000.0f + "[s]");
		System.out.println("<kameRecipe> Added the" + FINE + " recipe[s]");
		Bukkit.getLogger().log(FAIL > 0 ? Level.SEVERE : Level.INFO , "<kameRecipe> failed recipe is " + FAIL);
		System.out.println("<kameRecipe> ===============================================");
	}

	public void parseFile(File file) throws IOException {
		LINE = WARN = 0;
		filename = file.getName();
		String enc = getEncode(file);
		if(enc == null) throw new IOException("Unknown encode type!");
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), enc))) {
			Bukkit.getLogger().info("<kameRecipe> Loading to " + filename + "    EncodeType = " + enc);
			String line;
			while((line = br.readLine()) != null) {
				LINE++;
				if(line.matches("(CRAFT|COMBINE|SMELT|FUSION|SMASH) *.*")) {
					buildLine();//initialize
					createLine(line);
				}else {
					parseLine(line);
				}
			}
			Bukkit.getLogger().log(WARN > 0 ? Level.WARNING : Level.INFO , "<kameRecipe> Complete loaded " + filename + " " + WARN + " Warning");
			buildLine();//finalize
		}
	}
	
	private String getEncode(File file) {
		Path path = Paths.get(file.getPath());
		for (String c : new String[]{"ISO-2022-JP", "EUC-JP", "UTF-8", "UTF-16", "MS932"}) { 
			try {
				Files.readAllLines(path, Charset.forName(c));
			}catch(IOException e) {
				continue;
			}
			return c;
		}
		return null;
	}

	private void createLine(String line) {
		String str[] = line.split(" ", 2);
		switch(str[0]) {
		case "CRAFT":
			buffer = new BufferShapedRecipe(	str.length > 1 ? str[1] : "クラフト");
			break;
		case "COMBINE":
			buffer = new BufferShaplessRecipe(	str.length > 1 ? str[1] : "クラフト");
			break;
		case "SMELT":
			buffer = new BufferFurnaceRecipe(	str.length > 1 ? str[1] : "かまど");
			break;
		case "FUSION":
			buffer = new BufferFusionRecipe(	str.length > 1 ? str[1] : "醸造台");
			break;
		case "SMASH":
			buffer = new BufferSmashRecipe(		str.length > 1 ? str[1] : "金床");
			break;
		default:
		}
		READ = LINE;
	}

	private void parseLine(String line) {
		try {
			if(buffer != null && !buffer.isDrop())buffer.parseLine(line);
		} catch (IllegalItemException e) {
			WARN++;
			switch(e.getType()) {
			case AUGMENTS:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " AugmentError " + e.getError() + " too few arguments!! (" + filename + ")");
				break;
			case BADNBT:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " NBTError " + e.getError() + " is BAD NBT tag!! (" + filename + ")");
				break;
			case ENCHANTMENTS:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " NameError " + e.getError() + e.getMessages() +  " (" + filename + ")");
				break;
			case MATERIAL:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " NameErrorNameError " + e.getError() + " is unknown Material!! (" + filename + ")");
				break;
			case NUMBER:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " NumberFormatError " + e.getError() + " is no Number!! (" + filename + ")");
				break;
			case WARN:
				Bukkit.getLogger().severe("<kameRecipe> WARNING!! Line " + LINE + " " + e.getMessages() +  " (" + filename + ")");
				return;
			case OTHER:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " " + e.getMessages() +  " (" + filename + ")");
				break;
			default:
				break;
			}
			buffer.drop();
		}
	}

	private void buildLine() {
		if(buffer == null)return;
		if(buffer.isReady()) {
			buffer.createRecipe();
			FINE++;
		}else {
			Bukkit.getLogger().severe("<kameRecipe> Line " + READ + " " + buffer.getName() + " is not Added");
			Bukkit.getLogger().severe("<kameRecipe> Please make sure all elements put");
			FAIL++;
		}
		buffer = null;
	}
}
