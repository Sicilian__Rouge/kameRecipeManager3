package kame.kameRecipeManager.reader.utils;

public enum LineType {
	INPUT,
	OUTPUT,
	UNIQUE,
	OTHER,
}
