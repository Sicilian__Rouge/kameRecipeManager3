package kame.kameRecipeManager.reader.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;

public class Utils {

	@SuppressWarnings("deprecation")
	public static Material parseMaterial(String name) {
		String[] item = name.split(":");
		if(item[0].equals("AIR") || name.contains("minecraft:air"))return Material.AIR;
		if(item.length > 1)name = item[0].concat(":").concat(item[1]);
		Material material = Bukkit.getUnsafe().getMaterialFromInternalName(name.toLowerCase());
		if(material.equals(Material.AIR))material = Material.matchMaterial(item[0]);
		return material;
	}

	public static int toInt(String input, int def) {
		try {
			return Integer.parseInt(input);
		}catch(NumberFormatException e) {
			return def;
		}
	}

}
