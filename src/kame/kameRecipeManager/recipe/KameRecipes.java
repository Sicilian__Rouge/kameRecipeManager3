package kame.kameRecipeManager.recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.bukkit.Bukkit;

public class KameRecipes {
	private static HashMap<String, ArrayList<KRecipe>> recipes = new HashMap<>();
	private static final ArrayList<KRecipe> empty = new ArrayList<KRecipe>();


	public static ArrayList<KRecipe> getRecipe(String recipename) {
		return recipes.containsKey(recipename) ? recipes.get(recipename) : empty;
	}

	public static ArrayList<KRecipe> getRecipes(String... recipename) {
		ArrayList<KRecipe> list = new ArrayList<>();
		for(String recipe : recipename) {
			if(recipes.containsKey(recipe))list.addAll(recipes.get(recipe));
		}
		return list;
	}

	public static Map<String, ArrayList<KRecipe>> getAllRecipes() {
		return new TreeMap<String, ArrayList<KRecipe>>(recipes);
	}

	public static void addRecipe(String recipename, KRecipe recipe) {
		if(recipe.getBukkitRecipe() != null)Bukkit.addRecipe(recipe.getBukkitRecipe());
		ArrayList<KRecipe> list =  recipes.get(recipename);
		if (list == null)list = new ArrayList<KRecipe>();
		list.add(recipe);
		recipes.put(recipename, list);
	}

	public static void clear() {
		Bukkit.clearRecipes();
		recipes = new HashMap<>();
	}
}
