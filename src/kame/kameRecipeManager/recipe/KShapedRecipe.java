package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.ProductRecipe;

public class KShapedRecipe implements KRecipe {
	private Map<Character, ItemStack> map;
	private ShapedRecipe recipe;
	private List<String> option;
	private Set<CheckMode> check;
	private List<ProductRecipe> products;

	public KShapedRecipe(ShapedRecipe recipe, Map<Character, ItemStack> map, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.recipe = recipe;
		this.map = map;
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public String[] getShape() {
		return this.recipe.getShape();
	}

	public Map<Character, ItemStack> getIngredientMap() {
		return this.map;
	}

	public Recipe getBukkitRecipe() {
		return this.recipe;
	}

	public ItemStack getResult() {
		return this.recipe.getResult();
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}

}
