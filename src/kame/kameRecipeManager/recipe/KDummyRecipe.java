package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.ProductRecipe;

public class KDummyRecipe implements KRecipe {

	private final ItemStack item;

	public KDummyRecipe(ItemStack item) {
		this.item = item;
	}

	/**
	 * アイテム以外すべてNullを返すことに注意、除外レシピがこれに相当
	 */
	@Override
	public Recipe getBukkitRecipe() {
		return null;
	}

	@Override
	public ItemStack getResult() {
		return item;
	}

	@Override
	public List<String> getOption() {
		return null;
	}

	@Override
	public List<ProductRecipe> getProducts() {
		return null;
	}

	@Override
	public Set<CheckMode> getCheckMode() {
		return null;
	}

}
