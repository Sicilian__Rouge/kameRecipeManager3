package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.option.ProductRecipe;

public class KFurnaceRecipe implements KRecipe {
	private ItemStack input;
	private FurnaceRecipe recipe;
	private List<String> option;
	private int time;
	private CraftingEffect effect;
	private CraftingSound sound;
	private Set<CheckMode> check;
	private List<ProductRecipe> products;

	public KFurnaceRecipe(Recipe recipe, ItemStack item, int time, CraftingEffect effect, CraftingSound sound, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.input = item;
		this.time = time;
		this.effect = effect;
		this.sound = sound;
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public ItemStack getInput() {
		return this.input;
	}

	public int getCookTime() {
		return this.time;
	}

	public Recipe getBukkitRecipe() {
		return this.recipe;
	}

	public ItemStack getResult() {
		return this.recipe.getResult();
	}
	
	public CraftingEffect getEffect() {
		return effect;
	}

	public CraftingSound getSound() {
		return sound;
	}

	public List<String> getOption() {
		return this.option;
	}
	
	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}
}
