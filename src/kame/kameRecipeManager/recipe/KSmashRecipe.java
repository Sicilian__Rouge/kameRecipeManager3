package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.option.ProductRecipe;

public class KSmashRecipe implements KRecipe {

	private List<ItemStack> input;
	private String spell;
	private ItemStack result;
	private List<String> option;
	private CraftingEffect effect;
	private CraftingSound sound;
	private List<ProductRecipe> products;
	private Set<CheckMode> check;

	public KSmashRecipe(Recipe recipe, List<ItemStack> items, String spell, CraftingEffect effect, CraftingSound sound, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.input = items;
		this.spell = spell;
		this.effect = effect;
		this.sound = sound;
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public List<ItemStack> getInput() {
		return this.input;
	}

	public Recipe getBukkitRecipe() {
		return null;
	}

	public String getSpell() {
		return spell;
	}

	public ItemStack getResult() {
		return result;
	}

	public CraftingEffect getEffect() {
		return effect;
	}

	public CraftingSound getSound() {
		return sound;
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}

}
