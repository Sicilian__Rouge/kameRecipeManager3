package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.ProductRecipe;

public class KShapelessRecipe implements KRecipe {
	private List<ItemStack> items;
	private ShapelessRecipe recipe;
	private List<String> option;
	private List<ProductRecipe> products;
	private Set<CheckMode> check;

	public KShapelessRecipe(ShapelessRecipe recipe, List<ItemStack> items, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.recipe = recipe;
		this.items = items;
		this.option = option;
		this.products = products;
		this.check = check;
	}
	
	public List<ItemStack> getIngredientList() {
		return this.items;
	}

	public int getSize() {
		return items.size();
	}

	public Recipe getBukkitRecipe() {
		return this.recipe;
	}

	public ItemStack getResult() {
		return this.recipe.getResult();
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}

}
