package kame.kameRecipeManager.recipe.option;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;

public class CraftingSound {
	private Sound sound;
	private String soundforname = "";
	private float volume = 1;
	private float pitch = 1;
	private int tickper = 10;

	public CraftingSound() {}

	public CraftingSound(String sound, String... options) throws IllegalItemException {
		try {
			this.sound = Sound.valueOf(sound);
		}catch(IllegalArgumentException e) {
			if(sound.matches("[0-9]+")) {
				int value = Integer.parseInt(sound);
				this.sound = Effect.values().length <= value ? Sound.values()[value] : null;
			}else {
				soundforname = sound;
			}
		}
		for(String str : options) {
			String line[] = str.split("=", 2);
			if(line.length != 2) {
				throw new IllegalItemException(ErrorType.WARN, "", "SoundAugumentError ", "(", str, ")");
			}
			switch(line[0]) {
			case "volume"://volume=0.1
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ")");
				}
				this.pitch = Float.parseFloat(line[1]);
				break;
			case "pitch"://pitch=0.1
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ")");
				}
				this.pitch = Float.parseFloat(line[1]);
				break;
			case "tickper"://pitch=0.1
				if(!line[1].matches("[0-9]+")) {
					throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ")");
				}
				this.tickper = Integer.parseInt(line[1]);
				break;
			default:
				throw new IllegalItemException(ErrorType.WARN, "", "SoundTypeError ", "(", str, ")");
			}
		}
	}
	public void playSound(Location loc) {
		if((sound == null && soundforname == null) || (System.currentTimeMillis() / 50 % tickper) != 0)return;
		if(sound != null) {
			loc.getWorld().playSound(loc, sound, volume, pitch);
		}else {
			loc.getWorld().playSound(loc, soundforname, volume, pitch);
		}
	}

	public void playSound(Player player, Location loc) {
		if((sound == null && soundforname == null) || (System.currentTimeMillis() / 50 % tickper) != 0)return;
		if(sound != null) {
			player.playSound(loc, sound, volume, pitch);
		}else {
			player.playSound(loc, soundforname, volume, pitch);
		}
	}

}
