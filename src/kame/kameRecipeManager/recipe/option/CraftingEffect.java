package kame.kameRecipeManager.recipe.option;

import org.bukkit.Effect;
import org.bukkit.Location;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;

public class CraftingEffect {
	private Effect effect;
	private int id = 0;
	private int data = 0;
	private float speed = 0.1f;
	private float offset = 0;
	private int count = 1;
	private float offsetX = 0.5f;
	private float offsetY = 0.5f;
	private float offsetZ = 0.5f;
	private int tickper = 10;
	
	public CraftingEffect() {}

	public CraftingEffect(String effect, String... options) throws IllegalItemException {
		try {
			this.effect = Effect.valueOf(effect);
		}catch(IllegalArgumentException e) {
			if(effect.matches("[0-9]+")) {
				int value = Integer.parseInt(effect);
				this.effect = Effect.values().length <= value ? Effect.values()[value] : null;
			}
		}
		for(String str : options) {
			String data[];
			String line[] = str.split("=", 2);
			if(line.length != 2) {
				throw new IllegalItemException(ErrorType.WARN, "", "EffectAugumentError ", "(", str, ")");
			}
			switch(line[0]) {
			case "data"://data=0 0
				if(!line[1].matches("[0-9]+ [0-9]+")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				data = line[1].split(" ");
				if(data.length > 0)this.id   = Integer.parseInt(data[0]);
				if(data.length > 1)this.data = Integer.parseInt(data[1]);
				break;
			case "speed"://speed=0.1
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.speed = Float.parseFloat(line[1]);
				break;
			case "count"://count=10
				if(!line[1].matches("[0-9]+")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.count = Integer.parseInt(line[1]);
				break;
			case "color"://color=r,g,b
				if(!line[1].matches("[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				data = line[1].split(" ");
				this.offsetX = Float.parseFloat(data[0]);
				this.offsetY = Float.parseFloat(data[1]);
				this.offsetZ = Float.parseFloat(data[2]);
				break;
			case "vector"://vector=x,y,z
				if(!line[1].matches("[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				data = line[1].split(" ");
				this.offsetX = Float.parseFloat(data[0]);
				this.offsetY = Float.parseFloat(data[1]);
				this.offsetZ = Float.parseFloat(data[2]);
				break;
			case "offsety"://offset=y
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.offset = Float.parseFloat(line[1]);
				break;
			case "tickper"://pitch=0.1
				if(!line[1].matches("[0-9]+")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.tickper  = Integer.parseInt(line[1]);
				break;
			default:
				throw new IllegalItemException(ErrorType.WARN, "", "EffectTypeError ", "(", str, ")");
			}

		}
	}

	public void playEffect(Location loc) {
		if(effect == null || (System.currentTimeMillis() / 50 % tickper) != 0)return;
		loc.getWorld().spigot().playEffect(loc.add(0, offset, 0), effect, id, data, offsetX, offsetY, offsetZ, speed, count, 16);
	}
}
