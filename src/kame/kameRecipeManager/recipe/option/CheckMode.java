package kame.kameRecipeManager.recipe.option;

public enum CheckMode {
	uncheckname,
	uncheckenchant,
	uncheckitemflags,
	uncheckother,
	skipname,
	skiplore,
	skipnbt,
	;

	public static CheckMode fromName(String ckm) {
		try {
			return CheckMode.valueOf(ckm.toLowerCase());
		}catch(IllegalArgumentException ew) {
			return null;
		}
	}
}
