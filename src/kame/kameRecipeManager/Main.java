package kame.kameRecipeManager;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import kame.kameRecipeManager.command.RecipeCommand;
import kame.kameRecipeManager.listener.BlockListener;
import kame.kameRecipeManager.listener.CraftListener;
import kame.kameRecipeManager.reader.FileReader;

public class Main extends JavaPlugin implements Listener {

	private static Plugin instance;
	private static FileConfiguration config;
	private static float offset = 1;
	public static boolean debug = false;
	public static final char COLOR = ChatColor.COLOR_CHAR;
	public void onEnable() {
		instance = this;
		getServer().getPluginManager().registerEvents(new CraftListener(), this);
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		RecipeCommand r = new RecipeCommand();
		getCommand("kamerecipe").setExecutor(r);
		getCommand("kamerecipe").setTabCompleter(r);
		loadConfig();
		FileReader.readRecipes();
	}

	public void onDisable() {
	}

	public static void loadConfig() {
		instance.saveDefaultConfig();
		try {
			config = YamlConfiguration.loadConfiguration(new File(instance.getDataFolder(), "config.yml"));
			debug = config.getBoolean("debug");
			offset = Float.parseFloat(config.getString("offset-y"));
		} catch (Exception e) {
			instance.getLogger().warning("Config error! Configを確認してください！");
			instance.getLogger().warning(e.getMessage());
		}
	}

	public static void cast(Object o) {
		if(debug)Bukkit.broadcastMessage(String.valueOf(o));
	}

	public static float getOffsetY() {
		return offset;
	}

	public static Plugin getInstance() {
		return instance;
	}

	public static FileConfiguration getConf() {
		return config;
	}
}
