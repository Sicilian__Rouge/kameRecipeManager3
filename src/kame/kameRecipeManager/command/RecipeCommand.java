package kame.kameRecipeManager.command;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.reader.FileReader;
import kame.kameRecipeManager.recipe.KameRecipes;

public class RecipeCommand implements CommandExecutor, TabCompleter {

	private static HashSet<UUID> keeps = new HashSet<UUID>();

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command label, String cmd, String[] args) {
		if(sender.hasPermission("kamerecipe.user") && args.length == 1 && args[0].equals("keep") && (sender instanceof Player)) {
			if(keeps.contains(((Player) sender).getUniqueId())) {
				keeps.remove(((Player) sender).getUniqueId());
				sender.sendMessage(Main.getConf().getString("message.DispKeepOff"));
			} else {
				keeps.add(((Player) sender).getUniqueId());
				sender.sendMessage(Main.getConf().getString("message.DispKeepOn"));
			}
		}
		if(!sender.hasPermission("kamerecipe.admin"))return false;
		if(sender.isOp() && args.length == 1 && args[0].equals("list")) {
			for(String name : KameRecipes.getAllRecipes().keySet())
				sender.sendMessage("§a[kame.] " + "§r" + name + "§r" + " in " + KameRecipes.getRecipe(name).size() + " recipe[s]");
		}
		if(args.length == 1 && args[0].equals("reload")) {
			FileReader.readRecipes();
			Main.loadConfig();
			sender.sendMessage("[kame.] configReloaded!");
		}
		if(args.length > 0 && args[0].equals("debug")) {
			Main.debug = !Main.debug;
			sender.sendMessage("[kame.] DebugMode= " + Main.debug);
		}
		if(args.length > 1 && sender instanceof Player) {
			if(args[0].equals("sounds")) {
				try {
					Sound s = Sound.valueOf(args[1]);
					float volume = args.length > 2 && args[2].matches("[0-9](.[0-9]+)?") ? Float.parseFloat(args[2]) : 1; 
					float pitch = args.length > 3 && args[3].matches("[0-9](.[0-9]+)?") ? Float.parseFloat(args[3]) : 1; 
					if(s != null)((Player)sender).playSound(((Player)sender).getLocation(), s, volume, pitch);
					sender.sendMessage("[kame.] Sounds. ID= §e" + s.ordinal() + "§r Name= §e" + s.name());
				}catch(IllegalArgumentException e) {
					sender.sendMessage("[kame.] 選択されたものが見つかりませんでした");
				}

			}else
			if(args[0].equals("effects")) {
				try {
					Effect s = Effect.valueOf(args[1]);
					if(s != null)((Player)sender).playEffect(((Player)sender).getLocation(), s,
							args.length > 2 && args[2].matches("[0-9]+") ? Integer.parseInt(args[2]) : 1);
					sender.sendMessage("[kame.] Effects. ID= §e" + s.ordinal() + "§r Name= §e" + s.name());
				}catch(IllegalArgumentException e) {
					sender.sendMessage("[kame.] 選択されたものが見つかりませんでした");
				}
			}
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command label, String cmd, String[] args) {
		List<String> list = new ArrayList<String>();
		if(args.length == 2 && (sender.isOp() || sender.hasPermission("kamerecipe.admin"))) {
			if(args[0].equals("sounds")) {
				for(Sound s : Sound.values())if(s.toString().toLowerCase().startsWith(args[1].toLowerCase()))list.add(s.toString());
			}else
			if(args[0].equals("effects")) {
				for(Effect s : Effect.values())if(s.toString().toLowerCase().startsWith(args[1].toLowerCase()))list.add(s.toString());
			}else
			if(args[0].equals("items")) {
				for(Material s : Material.values())if(s.toString().toLowerCase().startsWith(args[1].toLowerCase()))list.add(s.toString());
			}
			if(args[0].equals("ench")) {
				for(Enchantment s : Enchantment.values())if(s.toString().toLowerCase().startsWith(args[1].toLowerCase()))list.add(s.toString());
			}
		}
		if((sender.isOp() || sender.hasPermission("kamerecipe.admin")) && args.length == 1) {
			String[] arg = {"reload", "list", "debug", "items", "sounds", "effects"};
			for(String str : arg)if(str.startsWith(args[0]))list.add(str);
		}
		if(args.length == 1 && sender.hasPermission("kamerecipe.user")) {
			String[] arg = {"keep"};
			for(String str : arg)if(str.startsWith(args[0]))list.add(str);
		}

		return list;
	}

}
