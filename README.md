Spigot用レシピプラグイン<br>
Bukkitでは動作しないので注意（Java8以降）<br>
<br>
--主なバニラからの仕様変更<br>
・Lore文のついたクラフト、金床での名前変更が出来なくなる<br>
・クラフトでツール同士を重ね合わせて修理することが不可能に<br>
・アイテムのNBTが変更されているブロックは設置時に保持されるように<br>
・エンティティがブロックを変更するとき上の処理が行われたブロックは変更をキャンセルするように<br>
<br>
--プラグインの仕様時の注意<br>
・クラフトの処理を上書きしているため、デフォルトのレシピを追加したりするプラグインとはほとんど競合すると思われます<br>
・1.9以降のみ対応予定です（サウンドファイル等）<br>
・予期せぬ不具合、謎い仕様がその他多々あります<br>
<br>
--レシピの作り方<br>
#通常の作業台にはあまり手を加えない仕様にしております<br>
#なので通常の作業台では特殊アイテムを普通のアイテムとしてのクラフトも許してしまうこともあります（Lore文で解決）<br>
#アイテムの説明文の部分に@craftとついているブロックが特殊作業台対象です<br>
#アイテムの説明文の部分に@furnaceとついているかまどが特殊かまど対象です<br>
#アイテムの説明文の部分に@fusionとついている醸造台が特殊調合台対象です<br>
#アイテムの説明文の部分に@smashとついている金床が特殊金床対象です<br>
#@craftは作業台以外にも置けるブロックならすべて対応していると思います<br>
#また@hideとつけると置いたときにネームタグを表示しなかったり@inHandで手持ち作業台になります<br><br>
<br>
<br>
--おてほん<br>
<br>
「#」とかそれ以外の適当な予約文字以外で始めてくれれば適当に読み飛ばしてくれます<br>
<br>
CRAFT ここに任意で作業題名<br>
1 + 1<br>
0 + 1<br>
1 = STONE @name:アイテム名 @lore:Lore文1行目 @lore:Lore文2行目 @ench:ID:power @tag:{データタグ(giveコマンドとかの)}<br>
=== DIAMOND @name:上と同じように指定可能<br><br>
<br>
こんな感じにクラフトオプション指定可能<br>
$option @playsound:ENEITY_WOLF_BARK:1:2<br>
<br>
COMBINE これで不定形レシピ<br>
I = STONE @name:ここでも同じようにアイテム指定可能<br>
<br>
後ろに数字をつけることで複数要求<br>
I3= SAND:1 @name:以下アイテム指定は全ての場所で同じように指定可能<br>
<br>
=== NETHER_STAR @name:安すぎない!? @lore:ダイジョーブだ<br>
<br>
オプションは複数行可能です見えにくかったらどんどん改行していきましょう<br>
$option @level:1000-1001 @failinfo:この作業台ではクラフトできません<br>
$option @failsound:BLOCK_NOTE_BASS<br>
<br>
カラーコードは§（セクション）で付けることが可能です<br>
<br>
SMELT ここも同じく名前<br>
入力は1つまで<br>
I = STICK<br>
=== COAL @name:コゲコゲ<br>
$times:100<br>
時間指定可能<br>
<br>
使用法はあまりかまどと変わらない<br>
最大3つまで入力は指定可能<br>
FUSION (ry<br>
I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10<br>
=== BONE @tag:{HideFlags:1} @name:§bほねっこ.v2 @lore:§aとてもおいしい(右クリックで試食) @lore:§a20/20 @ench:1:0<br>
$times:100<br>
<br>
--Itemオプションのリスト<br>
@name:\<name><br>
@lore:\<lore><br>
@ench:\<ench>:\<power><br>
@tag:{name:...}<br>
<br>
<br>
--Craftオプションのリスト<br>
@level:\<min-max><br>
@levelcost:\<cost><br>
@permission:\<perm><br>
@info:\<message><br>
@pathinfo:\<message><br>
@failinfo:\<message><br>
@broadcast:\<message><br>
@playsound:\<sound>:\<volume>:\<pitch><br>
@playeffect:\<effect>:\<id><br>
@failsound:\<sound>:<volume>:\<pitch><br>
@faileffect:\<effect>:\<id><br>
@pathsound:\<sound>:\<volume>:\<pitch><br>
@patheffect:\<effect>:\<id><br>
@failexpand:\<power>:\<true|false><br>
@pathexpand:\<power>:\<true|false><br>
@pathplcmd:\<command><br>
@failplcmd:\<command><br>
@pathclcmd:\<command><br>
@failclcmd:\<command><br>
@world:\<name><br>
@biome:\<name><br>
<br>

--------------以下適当にサンプルレシピ-------------<br><br><br><br>
######################################## 前提アイテム #########################################<br>
<br>
<br>
<br>
SMELT 粉砕機<br>
I = OBSIDIAN<br>
=== 289 @name:黒曜石の粉末 @lore:加熱注意 @ench:35:1<br>
$times:4000<br>
$effect:TILE_BREAK:49:0:0:0.2<br>
<br>
SMELT 圧縮機<br>
I = NETHER_BRICK_ITEM @name:鋼インゴット @lore:硬い鋼 @lore:なかなかの重量感 @ench:16:4<br>
=== IRON_PLATE @name:鉄の金型 @lore:鋼を圧縮して形成したもの @ench:0:1<br>
$times:4000<br>
<br>
<br>
1 = BRICK @name:耐熱壁 @lore:熱くなっても丈夫 @ench:0:1<br>
<br>
1 = OBSIDIAN @name:耐圧壁 @lore:高い圧力をかけても割れにくい @ench:0:1<br>
<br>
2 = FLINT @name:大粒の砥石 @lore:切り傷注意 @ench:0:1<br>
<br>
3 = IRON_PLATE @name:鉄の金型 @lore:鋼を圧縮して形成したもの @ench:0:1<br>
<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
######################################## 一般ツール #########################################<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
<br>
########### 粉砕機 ##########<br>
CRAFT<br>
1 + 1 + 1<br>
2 + 3 + 2<br>
4 + 4 + 4<br>
=== FURNACE @name:粉砕機 @lore:@furnace @lore:金属を砕くのに使用する @ench:0:1<br>
1 = IRON_SWORD<br>
2 = FLINT<br>
3 = FURNACE<br>
4 = STONE<br>
########### 加熱機 ##########<br>
<br>
CRAFT<br>
1 + 3 + 1<br>
1 + 2 + 1<br>
3 + 3 + 3<br>
=== FURNACE:0 @name:高温かまど @lore:@furnace @lore:物を溶かすのに使う @ench:0:1<br>
1 = 45<br>
2 = FURNACE<br>
3 = 289:0 @name:黒曜石の粉末 @lore:加熱注意 @ench:35:1<br>
$option @playsound:WOLF_BARK<br>
<br>
########### 圧縮機 ##########<br>
CRAFT<br>
1 + 2 + 1<br>
1 + 3 + 1<br>
4 + 4 + 4<br>
=== FURNACE @name:圧縮機 @lore:@furnace @lore:物を圧縮するときに使う @ench:0:1<br>
1 = OBSIDIAN<br>
2 = PISTON<br>
3 = STONE_PLATE<br>
4 = IRON_INGOT<br>
<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
######################################## 上級ツール#########################################<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
<br>
<br>
########## 粉砕機 #################<br>
CRAFT 機械設計台<br>
1 + 1 + 1<br>
2 + 3 + 2<br>
4 + 4 + 4<br>
=== FURNACE @name:破砕機 @lore:@furnace @lore:@burnrate:0.1 @ench:0:1 @lore:@burnrate:0.1 <br>
1 = DIAMOND_SWORD<br>
2 = FLINT @name:大粒の砥石 @lore:切り傷注意 @ench:0:1<br>
3 = FURNACE @name:粉砕機 @lore:@furnace @ench:0:1<br>
4 = IRON_BLOCK<br>
<br>
########## 高温かまど #################<br>
CRAFT 機械設計台<br>
1 + 3 + 1<br>
1 + 2 + 1<br>
3 + 3 + 3<br>
=== FURNACE @name:灼熱かまど @lore:@furnace @ench:0:1 @lore:@burnrate:0.1<br>
1 = BRICK @name:耐熱壁 @lore:熱くなっても丈夫 @ench:0:1<br>
2 = FURNACE @name:高温かまど @lore:@furnace @ench:0:1<br>
3 = 289 @name:黒曜石の粉末 @lore:加熱注意 @ench:35:1<br>
<br>
########### 圧縮機 ##########<br>
CRAFT 機械設計台<br>
1 + 2 + 1<br>
1 + 3 + 1<br>
4 + 4 + 4<br>
=== FURNACE @name:凝縮機 @lore:Furnace @ench:0:1 @lore:@burnrate:0.1<br>
1 = OBSIDIAN @name:耐圧壁 @lore:高い圧力をかけても割れにくい @ench:0:1<br>
2 = PISTON<br>
3 = IRON_PLATE @name:鉄の金型 @lore:鋼を圧縮して形成したもの @ench:0:1<br>
4 = IRON_BLOCK<br>
<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
######################################## 特上ツール #########################################<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
<br>
########## 粉砕機 #################<br>
CRAFT 機械設計台<br>
1 + 1 + 1<br>
2 + 3 + 2<br>
4 + 4 + 4<br>
=== FURNACE @name:回転粉砕機 @lore:@furnace @ench:0:1<br>
1 = EMERALD @name:§bネコライト鉱石 @lore:硬く耐衝撃性に優れた鉱石 @ench:34:10<br>
2 = FLINT @name:砂状の砥石 @lore:切り傷注意<br>
3 = IRON_BLOCK @name:マシンシャーシ @lore:特殊な機械の骨組み @ench:0:1<br>
4 = IRON_BLOCK<br>
<br>
########## 高温かまど #################<br>
<br>
CRAFT 機械設計台<br>
1 + 3 + 1<br>
1 + 2 + 1<br>
3 + 3 + 3<br>
=== FURNACE @name:爆熱かまど @lore:@furnace @ench:0:1 @lore:@burnrate:0.1<br>
1 = EMERALD @name:§bネコライト鉱石 @lore:硬く耐衝撃性に優れた鉱石 @ench:34:10<br>
2 = IRON_BLOCK @name:マシンシャーシ @lore:特殊な機械の骨組み @ench:0:1<br>
3 = SULPHUR @name:黒曜石の粉末 @lore:加熱注意 @ench:35:1<br>
<br>
########### 圧縮機 ##########<br>
CRAFT 機械設計台<br>
1 + 2 + 1<br>
1 + 3 + 1<br>
4 + 5 + 4<br>
=== FURNACE @name:凝縮機 @lore:Furnace @ench:0:1 @lore:@burnrate:0.1<br>
1 = EMERALD @name:§bネコライト鉱石 @lore:硬く耐衝撃性に優れた鉱石 @ench:34:10<br>
2 = PISTON<br>
3 = IRON_PLATE @name:鉄の金型 @lore:鋼を圧縮して形成したもの @ench:0:1<br>
4 = IRON_BLOCK<br>
5 = IRON_BLOCK @name:マシンシャーシ @lore:特殊な機械の骨組み @ench:0:1<br>
<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
######################################## 例外ツール #########################################<br>
/////////////////////////////////////////////////////////////////////////////////////////////<br>
<br>
########### 圧縮機 ##########<br>
CRAFT 精密作業台<br>
1 + 2 + 1<br>
1 + 3 + 1<br>
4 + 5 + 4<br>
=== FURNACE @name:爆縮機 @lore:Furnace @ench:0:1 @lore:@burnrate:0.1<br>
1 = OBSIDIAN @name:強化壁材 @lore:§c圧縮により対爆加工のされたブロック @lore:§c:無駄に硬い @ench:34:10<br>
2 = REDSTONE_BLOCK @name:§b爆縮用点火器 @lore:§b爆縮器の点火プラグ @lore:§b叩くと中まで詰まった音がする @ench:34:10<br>
3 = ANVIL @name:爆縮用ピストン @lore:§b金床を強化してできたピストン @lore:§b逆さに取り付けて使用する @ench34:10<br>
<br>
COMBINE §b作業台1<br>
I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10<br>
=== BARRIER @name:§bほねっこ食べないの...？ @lore:§aほんとに食べなくていいの？<br>
$option @playsound:ENEITY_WOLF_WHINE @level:1000<br>
<br>
COMBINE §b作業台2<br>
I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10<br>
=== BARRIER @name:§bほねっこ食べないの...？ @lore:§aほんとに食べなくていいの？<br>
$option @playsound:ENEITY_WOLF_WHINE @level:1000<br>
<br>
COMBINE §b作業台1<br>
I = BONE @name:練習用アイテム @lore:わんわんお<br>
=== BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10<br>
$option @pathplcmd:tp <player> ~ ~ ~13 @pathplcmd:xp -1000l @pathplcmd:summon FireworksRocketEntity ~ 67.8 ~ {LifeTime:0,FireworksItem:{id:401,Count:1,tag:{Fireworks:{Explosions:[{Flicker:1,Trail:1,Type:1,Colors:[16711680],FadeColors:[16776960]}]}}}}<br>
$option @playsound:ENEITY_WOLF_BARK:1:2<br>
<br>
COMBINE §b作業台1<br>
I = BONE @name:§b練習用アイテム.v2 @lore:わんわんお<br>
=== BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10<br>
$option @pathplcmd:tp <player> ~31 ~ ~-72 @pathplcmd:summon FireworksRocketEntity ~ 67.8 ~ {LifeTime:0,FireworksItem:{id:401,Count:1,tag:{Fireworks:{Explosions:[{Flicker:1,Trail:1,Type:1,Colors:[16711680],FadeColors:[16776960]}]}}}}<br>
$option @level:10-10 @playsound:ENEITY_WOLF_BARK:1:2 @failsound:BLOCK_NOTE_BASS @failinfo:§d[作業台たん] §6作業台の裏にボタンがあるよ！押してみてね！<br>
<br>
COMBINE §b作業台2<br>
I = BONE @name:練習用アイテム @lore:わんわんお<br>
=== BONE<br>
$option @level:1000-1001 @failinfo:この作業台ではクラフトできません @failsound:BLOCK_NOTE_BASS @playsound:ENTITY_WOLF_WHINE:1:1.5<br>
<br>
<br>
COMBINE §b作業台2<br>
I = BONE @name:練習用アイテム @lore:わんわんお<br>
=== BONE<br>
$option @level:1001-1002<br>
<br>
FUSION §6調合台<br>
I = BONE @name:§bほねっこ @lore:§aおいしい(右クリックで試食) @lore:§a10/10<br>
=== BONE @tag:{HideFlags:1} @name:§bほねっこ.v2 @lore:§aとてもおいしい(右クリックで試食) @lore:§a20/20 @ench:1:0<br>
$times:100<br>
<br>
COMBINE §b開封台<br>
I = CHEST @name:§a皮防具一式パック @lore:§b開封台でクラフトしよう！<br>
=== LEATHER_HELMET @lore:§6RuluTown<br>
$option @playsound:ENEITY_WOLF_BARK:1:2 @pathsound:ENTITY_PLAYER_LEVELUP:1:0.5<br>
+100% = LEATHER_CHESTPLATE @lore:§6RuluTown<br>
+100% = LEATHER_LEGGINGS @lore:§6RuluTown<br>
+100% = LEATHER_BOOTS @lore:§6RuluTown<br>
<br>
COMBINE §b開封台<br>
I = CHEST @name:§a鉄防具一式パック @lore:§b開封台でクラフトしよう！<br>
=== IRON_HELMET @lore:§6RuluTown<br>
$option @playsound:ENEITY_WOLF_BARK:1:2 @pathsound:ENTITY_PLAYER_LEVELUP:1:0.5<br>
+100% = IRON_CHESTPLATE @lore:§6RuluTown<br>
+100% = IRON_LEGGINGS @lore:§6RuluTown<br>
+100% = IRON_BOOTS @lore:§6RuluTown<br>
<br>
COMBINE §b開封台<br>
I = CHEST @name:§aダイヤ防具一式パック @lore:§b開封台でクラフトしよう！<br>
=== DIAMOND_HELMET @lore:§6RuluTown<br>
$option @playsound:ENEITY_WOLF_BARK:1:2 @pathsound:ENTITY_PLAYER_LEVELUP:1:0.5<br>
+100% = DIAMOND_CHESTPLATE @lore:§6RuluTown<br>
+100% = DIAMOND_LEGGINGS @lore:§6RuluTown<br>
+100% = DIAMOND_BOOTS @lore:§6RuluTown<br>

